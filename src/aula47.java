public class aula47 {  // caracteristicas dos arrays

    public static void main(String[] args) {

        int[] a = new int [10];  // nova instancia tipo int com 10 posições

        a[0] = 0; // variavel a, posição 0, valor 0 elemento
        a[1] = 111; // variavel a, posição 1, valor 111 elemento
        a[2] = 222; // variavel a, posição 2, valor 222 elemento
        a[3] = 333; // variavel a, posição 3, valor 333 elemento
        a[4] = 444; // variavel a, posição 4, valor 444 elemento
        a[5] = 555; // variavel a, posição 5, valor 555 elemento

        System.out.println(a[4]); // imprimir 4 posição do array, valor
        System.out.println(a[5]); // imprimir 5 posição do array, valor
        System.out.println(a[6]); // imprimir 6 posição do array, valor
    }
}
